<?php

namespace App\Http\Controllers;

use App\Library\Cart;
use App\Library\Response\Response;
use \Illuminate\Http\Response as HTTPCode;
use Illuminate\Http\Request;

class CartController extends Controller
{
    use Response;

    /** @var Request */
    protected $request;

    /** @var Cart */
    protected $cart;

    /**
     * Create a new controller instance.
     */
    public function __construct(Request $request, Cart $cart)
    {
        $this->request = $request;
        $this->cart = $cart;
        $this->sessionKey = session();
    }

    /**
     * Adds a given product to the shopping cart.
     *
     * @return string The shopping cart info
     */
    public function add() : string
    {
        $this->request->validate([
            'product_id' => 'required|alphanum|max:8',
        ]);

        $productId = $this->request->get('product_id');
        $result = $this->cart->withSession($this->sessionKey->getId())->add($productId);
        return $this->response(HTTPCode::HTTP_OK, $result);

    }

    /**
     * remove
     *
     * @return string
     */
    public function remove() : string
    {
        $this->request->validate([
            'product_id' => 'required|alphanum|max:8',
        ]);

        $productId = $this->request->get('product_id');
        $result = $this->cart->withSession($this->sessionKey->getId())->remove($productId);
        return $this->response(HTTPCode::HTTP_OK, $result);
    }

    /**
     * adjust
     *
     * @param $productId
     * @return string
     */
    public function adjust() : string
    {
        $this->request->validate([
            'product_id' => 'required|alphanum|max:8',
            'product_quantity' => 'required|numeric'
        ]);


        $productId = $this->request->get('product_id');
        $productQuantity = $this->request->get('product_quantity');

        $result = $this->cart->withSession($this->sessionKey->getId())->adjust($productId, $productQuantity);

        return $this->response(HTTPCode::HTTP_OK, $result);
    }

    /**
     * calculate
     *
     * @return string
     */
    public function calculate() : string
    {
        $result = $this->cart->withSession($this->sessionKey->getId())->calculate();
        return $this->response(HTTPCode::HTTP_OK, $result);
    }

}
