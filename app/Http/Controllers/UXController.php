<?php

namespace App\Http\Controllers;

use App\Library\Response\Response;
use \Illuminate\Http\Response as HTTPResponse;
use Illuminate\Http\RedirectResponse;

class UXController extends Controller
{
    use Response;

    /**
     *
     */
    public function shoppingcart() : string
    {
        return view('ecommerce');
    }

}
