<?php
namespace App\Library;

interface ICart
{
    /**
     * add
     *
     * @param string $productId The sku of the product as found in the basket
     *
     * @return stdClass with updated data about the basket state
     */
    public function add(string $productId) : \stdClass;

    /**
     * add
     *
     * @param string $productId The sku of the product as found in the basket
     *
     * @return stdClass with updated data about the basket state
     */
    public function remove(string $productId) : \stdClass;

    /**
     * adjust
     *
     * @param string $productId The sku of the product as found in the basket
     * @param int $productQuantity
     *
     * @return stdClass with updated data about the basket state
     */
    public function adjust(string $productId, int $productQuantity) : \stdClass;

    /**
     * Calculates the totals in the cart
     *
     * @param string $productId
     *
     * @return stdClass with updated data about the basket state
     */
    public function calculate() : \stdClass;
}