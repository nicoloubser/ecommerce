<?php
namespace App\Library\Repositories\Eloquent;

use App\Library\Models\Products;
use App\Library\Repositories\Contracts\ProductsInterface;
use Illuminate\Database\Eloquent\Collection;

class ProductsRepository implements ProductsInterface
{
    public function findBySKU(array $products) : ?Collection
    {
        return Products::wherein('sku', $products)->get();
    }
}