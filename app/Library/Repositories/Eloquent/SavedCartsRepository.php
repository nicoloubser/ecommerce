<?php
namespace App\Library\Repositories\Eloquent;

use App\Library\Models\SavedCarts;
use App\Library\Repositories\Contracts\SavedCartsInterface;
use Illuminate\Database\Eloquent\Collection;

class SavedCartsRepository implements SavedCartsInterface
{
    public $session_id;
    public $cart;
    public $expires;

    public function getCartBySessionId(string $sessionId) : ?SavedCarts
    {
       return SavedCarts::where('session_id', '=', $sessionId)->first();
    }

    public function save() : bool
    {
        try {
            $savedCarts = app(SavedCarts::class);
            $savedCarts->session_id = $this->session_id;
            $savedCarts->cart = $this->cart;
            $savedCarts->expires = $this->expires;
            $savedCarts->save();
        } catch (\Throwable $e) {
            return false;
        }
        return true;
    }

}