<?php

namespace App\Library\Repositories\Contracts;


use Illuminate\Database\Eloquent\Collection;

interface ProductsInterface
{
    public function findBySKU(array $products) : ?Collection;
}