<?php
namespace App\Library\Repositories\Contracts;

use App\Library\Models\SavedCarts;


interface SavedCartsInterface
{
    public function getCartBySessionId(string $sessionId) : ?SavedCarts;

    public function save() : bool;

}