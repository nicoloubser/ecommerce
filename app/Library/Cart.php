<?php
namespace App\Library;


use App\Library\Repositories\Eloquent\ProductsRepository;
use App\Library\Repositories\Eloquent\SavedCartsRepository;

class Cart implements ICart
{
    /** @var Products */
    protected $productsRepository;
    /** @var SavedCartsRepository */
    protected $savedCartsRepository;

    protected $sessionId;

    public function __construct(SavedCartsRepository $savedCarts, ProductsRepository $products)
    {
        $this->savedCartsRepository = $savedCarts;
        $this->productsRepository = $products;
    }

    /**
     * add
     *
     * @param string $productId The sku of the product as found in the basket
     *
     * @return stdClass for jsonencoding with updated data about the basket state
     */
    public function add(string $productId) : \stdClass
    {
        $cart = $this->savedCartsRepository->getCartBySessionId($this->sessionId);

        if(empty($cart)) {
            $this->savedCartsRepository->session_id = $this->sessionId;
            $this->savedCartsRepository->cart = json_encode([$productId => 1]);
            $this->savedCartsRepository->expires = date('Y-m-d H:i:s');
            $this->savedCartsRepository->save();

            return (object)[$productId => 1];
        }

        $basket = json_decode($cart->cart);
        $basket->$productId = isset($basket->$productId) ? $basket->$productId + 1 : 1;
        $cart->cart = json_encode($basket);
        $cart->save();

        return $basket;
    }

    /**
     * add
     *
     * @param string $productId The sku of the product as found in the basket
     *
     * @return stdClass for jsonencoding with updated data about the basket state
     */
    public function remove(string $productId) : \stdClass
    {
        $cart = $this->savedCartsRepository->getCartBySessionId($this->sessionId);

        if (empty($cart)) {
            return new \stdClass;
        }

        $basket = json_decode($cart->cart);

        if (isset($basket->$productId) && $basket->$productId >= 1) {
            $basket->$productId--;
        }

        $cart->cart = json_encode($basket);
        $cart->save();

        return $basket;
    }

    /**
     * adjust
     *
     * @param string $productId The sku of the product as found in the basket
     * @param int $productQuantity
     *
     * @return stdClass for jsonencoding with updated data about the basket state
     */
    public function adjust(string $productId, int $productQuantity) : \stdClass
    {
        $cart = $this->savedCartsRepository->getCartBySessionId($this->sessionId);

        if (empty($cart)) {
            $this->savedCartsRepository->session_id = $this->sessionId;
            $this->savedCartsRepository->cart = json_encode([$productId => $productQuantity]);
            $this->savedCartsRepository->expires = date('Y-m-d H:i:s');
            $this->savedCartsRepository->save();

            return [$productId => $productQuantity];
        }

        $basket = json_decode($cart->cart);
        $basket->$productId = isset($basket->$productId) ? $basket->$productId + $productQuantity : $productQuantity;
        $cart->cart = json_encode($basket);
        $cart->save();

        return  $basket;
    }

    /**
     * Calculates the totals in the cart
     *
     * @param string $productId
     *
     * @return stdClass for jsonencoding with updated data about the complete basket state amd totals
     */
    public function calculate() : \stdClass
    {
        $quantities = [];
        $totals = [];
        $totals['total'] = 0;
        $cart = $this->savedCartsRepository->getCartBySessionId($this->sessionId);

        if (empty($cart)) {
            return new \stdClass;
        }

        $basket = json_decode($cart->cart);

        // Build the products as
        foreach($basket as $productSKU => $selectedQuantity) {
            $quantities[$productSKU] = $selectedQuantity;
        }

        $selectedProducts = $this->productsRepository->findBySKU(array_keys($quantities));

        foreach($selectedProducts as $product) {
            $totals[$product->name]['price'] = ($quantities[$product->sku] * $product->price_cents)/100;
            $totals[$product->name]['quantity'] = $quantities[$product->sku];
            $totals['total'] += $quantities[$product->sku] * $product->price_cents;
        }
        $totals['total'] = $totals['total']/100;

        return (object)$totals;
    }

    /**
     * Sets the session to be used for the cart
     *
     * @param $sessionId
     *
     * @return $this
     */
    public function withSession($sessionId) : Cart
    {
        $this->sessionId = $sessionId;
        return $this;
    }
}