<?php
namespace App\Library\Response;

trait Response
{

    /**
     * Formalised response to ensure single and predictable response format
     *
     * @param int $responseCode
     * @param string $data
     * @return string
     *
     */
    public function response($responseCode = 200, $data = '') : string
    {
        $return['message'] = !empty($data) ? $data : 'No response data';
        $return['errors'] = '{}';
        return json_encode($return);
    }
}