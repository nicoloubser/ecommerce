<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<title>Shopping cart</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>


<script language="JavaScript">

$( document ).ready(function() {

    var productUpdate;

    $(".add").click(function(){
        productUpdate = "#"+$(this).data('id');
       $.post("/add", {product_id: $(this).data('id')}, function(data){
          $(productUpdate).html(data);
       }).fail(function(data) {
           $(productUpdate).html(data)
       });
    });

    $(".remove").click(function() {
        productUpdate = "#"+$(this).data('id');
        $.ajax({
            url: '/remove',
            type: 'DELETE',
            data: {product_id: $(this).data('id')},
            success: function (data) {
                $(productUpdate).html(data);
                console.log("Remove data returned " + data);
            },
            error: function (data) {
                $(productUpdate).html(data);
                console.log("Remove data returned " + data);
            },
        });
    });

    $(".adjust").keypress(function(event) {
        productUpdate = "#"+$(this).data('id');
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $.ajax({
                url: '/adjust',
                type: 'PATCH',
                data: {product_id: $(this).data('id'), product_quantity: $(this).val()},
                success: function (data) {
                    $(productUpdate).html(data);
                }
            });
        }
        event.stopPropagation();
    });

    $("#calculate").click(function() {
        $.ajax({
            url: '/calculate',
            type: 'get',
            success: function (data) {
                console.log('asdasdasdasdasda');
                $('#calculate_totals').html(data);
            }
        });
    });
});
</script>

<a href="#" class="btn btn-primary" id="calculate">Calculate totals</a>
<div id="calculate_totals">
</div>

<div align="center">
<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Bottle</h5>
                <p class="card-text">Glass bottle to store things in</p>
                <a href="#" class="btn btn-primary add" data-id="aba6785f">Add to cart</a>
                <a href="#" class="btn btn-primary remove" data-id="aba6785f">Remove from cart</a>
                <h6 class="card-title">Enter amount to add and press enter</h6>
                <input type="text" data-id="aba6785f" class="adjust">
                <div id="aba6785f"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Shoes</h5>
                <p class="card-text">Shoes for outside weather</p>
                <a href="#" class="btn btn-primary add" data-id="be2177d2">Add to cart</a>
                <a href="#" class="btn btn-primary remove" data-id="be2177d2">Remove from cart</a>
                <h6 class="card-title">Enter amount to add and press enter</h6>
                <input type="text" data-id="be2177d2" class="adjust">
                <div id="be2177d2"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Pencils</h5>
                <p class="card-text">Pencils to write things down with</p>
                <a href="#" class="btn btn-primary add" data-id="ce3e6c53">Add to cart</a>
                <a href="#" class="btn btn-primary remove" data-id="ce3e6c53">Remove from cart</a>
                <h6 class="card-title">Enter amount to add and press enter</h6>
                <input type="text" id="aba6785f" data-id="ce3e6c53" class="adjust">
                <div id="ce3e6c53"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Coat</h5>
                <p class="card-text">Coat for winter</p>
                <a href="#" class="btn btn-primary add" data-id="ab454450">Add to cart</a>
                <a href="#" class="btn btn-primary remove" data-id="ab454450">Remove from cart</a>
                <h6 class="card-title">Enter amount to add and press enter</h6>
                <input type="text" id="aba6785f" data-id="ab454450" class="adjust">
                <div id="ab454450"></div>
            </div>
        </div>
    </div>
</div>
</div>

























</html>





