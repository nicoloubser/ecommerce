<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saved_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('session_id', 50)->comment('The session identifying the shopping cart');
            $table->json('cart')->comment('The contents of the cart');
            $table->dateTime('expires')->comment('The expiry date of the cart');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saved_carts');
    }
}
