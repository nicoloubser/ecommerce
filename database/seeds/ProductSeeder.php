<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run()
    {

        $products = app(\App\Library\Models\Products::class);
        $products->sku = 'aba6785f';
        $products->name = 'Bottle';
        $products->description = 'Glass bottle to store things in';
        $products->price_cents = 2000;
        $products->save();

        $products = app(\App\Library\Models\Products::class);
        $products->sku = 'be2177d2';
        $products->name = 'Shoes';
        $products->description = 'Shoes for outside weather';
        $products->price_cents = 5000;
        $products->save();

        $products = app(\App\Library\Models\Products::class);
        $products->sku = 'ce3e6c53';
        $products->name = 'Pencils';
        $products->description = 'Pencils to write things down with';
        $products->price_cents = 200;
        $products->save();

        $products = app(\App\Library\Models\Products::class);
        $products->sku = 'ab454450';
        $products->name = 'Coat';
        $products->description = 'Coat for winter';
        $products->price_cents = 1400;
        $products->save();

    }

}