## To install
* Create a copy of the .env-example file and name it .env
* Run composer
* Run php artisan key:generate
* Create a database, add details to .env
* Run php artisan migrate. This will create the correct tables to the DB.
* Run php artisan db:seed, this will populate the DB with some products.

## To use

Use curl or postman, or the frontend provided.

At this stage the frontend is basic, and more a tool to help understand  the backend.

There is 1 calculate button, as well as 2 distinct buttons and one distinct input field per item.
* Calculate totals
* * returns the cart total
* Add to cart
* * Adds one product to the cart
* Remove from cart
* * Removes one product from the cart
* Input field "Enter amount to add and press enter"
* * Type amount and hit enter

The returning JSON is displayed upon return, otherwise hit F12 in the browser and check network traffic.

## API return data
THe API sends back data in a standard format. AMounts are always in cents.
{message:{}, errors:{}}

For example

When adding and removing data. The 8 digit hex value is the product SKU, and the amount in the users basket
{"message":{"aba6785f":4,"ce3e6c53":3},"errors":"{}"}

Viewing the cart
Total here represents the total price, and then a more verbose description of the cart.
{"message":{"total":86,"Bottle":{"price":80,"quantity":4},"Pencils":{"price":6,"quantity":3}},"errors":"{}"}


## To note
Most of the important files have been placed in the Library directory so that they can easily be found.
The rest are in controllers, web.php etc.

The api endpoints lives in web. THe web routes supplies me automatically with what I need
for my session handling. I understand session handling is not really Restfull, but I thought it
would be more lightweight than JWT tokens for this example technical challenge.
