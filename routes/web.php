<?php

$router->post('add', function() {
    $controller = app(\App\Http\Controllers\CartController::class);
    return $controller->add();
});

$router->delete('remove', function() {
    $controller = app(\App\Http\Controllers\CartController::class);
    return $controller->remove();
});

$router->patch('adjust', function() {
    $controller = app(\App\Http\Controllers\CartController::class);
    return $controller->adjust();
});

$router->get('calculate', function() {
    $controller = app(\App\Http\Controllers\CartController::class);
    return $controller->calculate();
});

$router->get('/', function() {
    return (new \App\Http\Controllers\UXController())->shoppingcart();
});